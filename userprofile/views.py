
from django.shortcuts import render_to_response
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from forms import UserProfileForm
from django.contrib.auth.decorators import login_required # checks to see whether the user is logged in
# if not the user is redirected to the login page
from django.contrib.auth.models import User
from userprofile.models import UserProfile
from article.models import Article
from friendship.models import Friend, FriendshipRequest, Follow
from django.contrib import messages
from notification.models import General_notification
from django.conf import settings
from userprofile.models import UserProfile

@login_required
def user_profile(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/loggedin')

        form = UserProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/home')
    
    else:
        user = request.user # shortcut to getting user
        profile = user.profile
        form = UserProfileForm(instance=profile)
    
    args = {}
    args.update(csrf(request))
    
    args['form'] = form

    
    return render_to_response('profile.html', args)
        



    args['user'] = request.user
    args['g_notification'] = General_notification.objects.all().filter(user=request.user, viewed=False)
    args['friend_request'] = FriendshipRequest.objects.all().filter(to_user=request.user)
    return render_to_response('profile.html', args)
    
@login_required     
def people_profile(request, profile_id):
    
    friend_list = []
    a = User.objects.get(id=profile_id)
    b = UserProfile.objects.get(user_id=a.id)
    c = Article.objects.filter(user=a.id)
    d = request.user
    e = Friend.objects.are_friends(request.user, a)
    fu = Follow.objects.follows(request.user, a)
    g = FriendshipRequest.objects.all().filter(from_user=request.user, to_user=a)
    
    friends = Friend.objects.friends(a)
    
    for f in friends:
        friend_list.append(f)
    
    args = {}
    args.update(csrf(request))
    args['username'] = a.username
    args['p_friend'] = a
    args['user_info'] = b
    args['articles'] = c
    args['user'] = d
    args['mutual_friends'] = e
    args['following'] = fu
    args['friends'] = g
    args['friend_request'] = FriendshipRequest.objects.all().filter(to_user=request.user)
    args['g_notification'] = General_notification.objects.filter(user=request.user, viewed=False)
    args['friend_list'] = UserProfile.objects.all().filter(user__in=friend_list)
    
    
    return render(request, 'people_profile.html', args)
    
def add_friend(request, user_id):

    username = request.POST.get('username')
    other_user = User.objects.get(username=username)
    Friend.objects.add_friend(request.user, other_user)
    messages.add_message(request, messages.SUCCESS, "Friend request sent")
    
    return HttpResponseRedirect('/accounts/user/%s' % user_id)

def follow(request, user_id):

    username = request.POST.get('username')
    other_user = User.objects.get(username=username)
    Follow.objects.add_follower(request.user, other_user)
    messages.add_message(request, messages.SUCCESS, "You are now following %s" % other_user.username)
    
    return HttpResponseRedirect('/accounts/user/%s' % user_id)

def unfriend(request, user_id):

    username = request.POST.get('username')
    other_user = User.objects.get(username=username)
    Friend.objects.remove_friend(other_user, request.user)
    messages.add_message(request, settings.DANGER_MESSAGE, "Friend Removed")
    
    return HttpResponseRedirect('/accounts/user/%s' % user_id)

def unfollow(request, user_id):

    username = request.POST.get('username')
    other_user = User.objects.get(username=username)
    Follow.objects.remove_follower(request.user, other_user)
    messages.add_message(request, settings.DANGER_MESSAGE, "No longer following %s" % other_user.username)
    
    return HttpResponseRedirect('/accounts/user/%s' % user_id)

@login_required
def friend_list(request, user_id):
    
    friend_list = []
    user_friends = User.objects.get(id=user_id)
    all_friends = Friend.objects.friends(user_friends)
    
    
    for friends in all_friends:
        friend_list.append(friends)
    
    args = {}
    args.update(csrf(request))
    args['all_friends'] = all_friends
    args['user'] = request.user
    args['g_notification'] = General_notification.objects.all().filter(user=request.user, viewed=False)
    args['friend_request'] = FriendshipRequest.objects.all().filter(to_user=request.user)
    args['friend_info'] = UserProfile.objects.all().filter(user__in=friend_list)
    return render_to_response('friend_list.html', args)
   
    
    
    

