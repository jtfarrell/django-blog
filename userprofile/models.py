from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

import logging
logr = logging.getLogger(__name__)

class UserProfile(models.Model):
    user = models.OneToOneField(User) # tie ins relationship with user model. meaning one user cn only have one ser profile
    likes_cheese = models.BooleanField(default=False)
    favourite_hamster_name = models.CharField(max_length=50) # favourite spelt wrong
    
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

@receiver(post_save, sender=User)
def add_userprofile(sender, **kwargs):
    print kwargs
    if kwargs.get('created', False): # False is a default value
        # if it hasn't been created default of false will be returned and the rest will be ignored
        print kwargs
        up =  UserProfile.objects.create(user=kwargs.get('instance'))
        logr.debug("Userprofile created %s" % up)

from time import time
from django.conf import settings
from blog import settings


class Choices(models.Model):
    choices = models.CharField(max_length=300)
    
    def __str__(self):
        return self.choices

def get_upload_file_name(instance, filename):
    return "assets/profile_pic_files/%s_%s" % (str(time()).replace('.','_'), filename)
    
    

class UserProfile(models.Model):
    user = models.OneToOneField(User) # tie ins relationship with user model. meaning one user cn only have one ser profile
    bio = models.TextField()
    profile_pic = models.FileField(upload_to=get_upload_file_name)
    genre = models.ManyToManyField(Choices)
    
    def get_thumbnail(self):
        thumb = str(self.profile_pic)
        if not settings.DEBUG:
            thumb = thumb.replace('assets/', '')
            
        return thumb
        
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

