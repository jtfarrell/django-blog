# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import userprofile.models


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0010_remove_userprofile_interests'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='profile_pic',
            field=models.FileField(default=b'profile_pic_files/default.jpg', upload_to=userprofile.models.get_upload_file_name),
            preserve_default=True,
        ),
    ]
