# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import userprofile.models


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0007_auto_20150128_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='bio',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='interests',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='profile_pic',
            field=models.FileField(upload_to=userprofile.models.get_upload_file_name),
            preserve_default=True,
        ),
    ]
