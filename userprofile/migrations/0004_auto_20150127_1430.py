# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import userprofile.models


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0003_auto_20150116_1823'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='favourite_hamster_name',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='likes_cheese',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='bio',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='interests',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='profile_pic',
            field=models.FileField(null=True, upload_to=userprofile.models.get_upload_file_name),
            preserve_default=True,
        ),
    ]
