from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from blog.forms import ContactForm1, ContactForm2, ContactForm3
from blog.views import ContactWizard
from django.conf import settings



urlpatterns = patterns('',
    (r'^articles/', include('article.urls')), # maps through to article Dir urls.py
    (r'^accounts/', include('userprofile.urls')),
    (r'^notification/', include('notification.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'blog.views.login'),
    url(r'^accounts/auth/$', 'blog.views.auth_view'),
    url(r'^accounts/logout/$', 'blog.views.logout'),
    url(r'^accounts/home/$', 'blog.views.home'),
    url(r'^accounts/invalid/$', 'blog.views.invalid_login'),
    url(r'^accounts/friend_requests/$', 'blog.views.friend_requests'),
    url(r'^accounts/friendship_accept/(?P<friendship_request_id>\d+)/$', 'blog.views.friendship_accept'),
    url(r'^accounts/friendship_reject/(?P<friendship_request_id>\d+)/$', 'blog.views.friendship_reject'),
    url(r'^accounts/friend_list/$', 'blog.views.friend_list'),
    #url(r'^accounts/follow/(?P<followee_id>\d+)/$', 'blog.views.follow'),
    url(r'^$', 'blog.views.register_user'),
    url(r'^accounts/register_success/$', 'blog.views.register_success'),
    url(r'contact/$', ContactWizard.as_view([ContactForm1, ContactForm2, ContactForm3])),
    url(r'^search/', include('haystack.urls')),
    
    
)



#

# when using include wrap using brackets instead of url keyword