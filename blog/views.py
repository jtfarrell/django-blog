from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth # confirms if user is logged in or out
from django.core.context_processors import csrf # web security
from forms import MyRegistrationForm # imports from forms.py
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.mail import send_mail
from userprofile.models import UserProfile
from django.contrib.auth.models import User
from article.models import Article
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from notification.models import Notification, General_notification
from friendship.models import FriendshipRequest, Friend, Follow
from django.contrib import messages
from django.conf import settings

import logging
logr = logging.getLogger(__name__)

def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('login.html', c)
    
def auth_view(request):
    username = request.POST.get('username', '') # gets post info from form
    password = request.POST.get('password', '') # empty strings means if there is nothing to get fromt he form, it will return a empty string
    user = auth.authenticate(username=username, password=password) # if it finds a match 'user' will be returned. if not None returned
    
    if user is not None:
        auth.login(request, user) # sets login status to logged in
        return HttpResponseRedirect('/accounts/home')

    else:
        return HttpResponseRedirect('/accounts/invalid')

@login_required
def home(request):
    
    friend_list = []
    n = Notification.objects.filter(user=request.user, viewed=False)
    friends = Friend.objects.friends(request.user)
    
    for f in friends:
        friend_list.append(f)
        
    
    args = {}
    args.update(csrf(request))
    
    args['full_name'] = request.user.username
    
    if UserProfile.objects.filter(user_id=request.user.id).exists() == True:
        args['user_info'] = UserProfile.objects.get(user_id=request.user.id)
        args['user'] = request.user
        args['articles'] = Article.objects.all().filter(user=request.user.id)
        args['friends'] = Friend.objects.friends(request.user)
        args['friend_request'] = FriendshipRequest.objects.all().filter(to_user=request.user)
        args['g_notification'] = General_notification.objects.all().filter(user=request.user, viewed=False)
        args['friend_list'] = UserProfile.objects.all().filter(user__in=friend_list)[:4]
        print Friend.objects.friends(request.user)
        return render_to_response('home.html', args)
    else:
        return render_to_response('home.html',
                                     {'full_name': request.user.username,
                                      'notifications': n})
        
def friend_requests(request):
    args = {}
    args.update(csrf(request))
    args['requests'] = FriendshipRequest.objects.all().filter(to_user=request.user)
    args['user'] = request.user
    args['g_notification'] = General_notification.objects.all().filter(user=request.user, viewed=False)
    return render_to_response('friend_requests.html', args)
    
def friendship_accept(request, friendship_request_id):

    accept_request = FriendshipRequest.objects.get(pk=friendship_request_id)
    follow_id = User.objects.get(id=accept_request.from_user.id)
    
    if Follow.objects.filter(follower=request.user, followee=follow_id).exists() == False:
        following_created = Follow.objects.add_follower(request.user, follow_id)
        
    if Follow.objects.filter(follower=follow_id, followee=request.user).exists() == False:
        followee_created = Follow.objects.add_follower(follow_id, request.user)
        
    accept_request.accept()
    messages.add_message(request, messages.SUCCESS, "Friendship accepted")
    return HttpResponseRedirect('/accounts/friend_requests/')

    return HttpResponseRedirect('/accounts/home/')

    
def friendship_reject(request, friendship_request_id):
    
    reject_request = FriendshipRequest.objects.get(pk=friendship_request_id)
    reject_request.reject()
    reject_request.delete()
    messages.add_message(request, settings.DANGER_MESSAGE, "Friendship rejected")
    return HttpResponseRedirect('/accounts/friend_requests/')

    return HttpResponseRedirect('/accounts/home/')

@login_required
def friend_list(request):
    
    all_friends = Friend.objects.friends(request.user)
    
    args = {}
    args.update(csrf(request))
    args['all_friends'] = all_friends
    args['user'] = request.user
    args['g_notification'] = General_notification.objects.all().filter(user=request.user, viewed=False)
    args['friend_request'] = FriendshipRequest.objects.all().filter(to_user=request.user)
    return render_to_response('friend_list.html', args)

                             
def invalid_login(request):
    return render_to_response('invalid_login.html')

def logout(request):
    auth.logout(request)
    return render_to_response('logout.html')

def register_user(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST) # creates object
        if form.is_valid(): 
            form.save()
            return HttpResponseRedirect('/accounts/register_success')
    
    args = {}
    args.update(csrf(request))
    
    args['form'] = MyRegistrationForm()
    
    return render_to_response('register.html', args)
        
def register_success(request):
    return render_to_response('register_success.html')
    
class ContactWizard(SessionWizardView):
    template_name = "contact_form.html"
    
    def done(self, form_list, **kwargs):
        form_data = process_form_data(form_list)
        
        return render_to_response('done.html', {'form_data': form_data})
    
    
def process_form_data(form_list):
    form_data = [form.cleaned_data for form in form_list]
    
    logr.debug(form_data[0]['subject'])
    logr.debug(form_data[1]['sender'])
    logr.debug(form_data[2]['message'])
    
    send_mail(form_data[0]['subject'], 
              form_data[2]['message'], form_data[1]['sender'],
              ['jimi_farrell@hotmail.co.uk'], fail_silently=False)
    
    return form_data
    
                             