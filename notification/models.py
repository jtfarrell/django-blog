from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from friendship.signals import friendship_request_accepted
from django.db.models import signals


class Notification(models.Model):
    title = models.CharField(max_length=256)
    message = models.TextField()
    viewed = models.BooleanField(default=False)
    user = models.ForeignKey(User)
    



                                    
                                    

class General_notification(models.Model):
    title = models.CharField(max_length=256)
    message = models.TextField()
    viewed = models.BooleanField(default=False)
    user = models.ForeignKey(User)
    
@receiver(post_save, sender=User) #the next function is going to respnd to post_save from the user model. as soon as the user does a post_save signal through the signal and it will activate the function below
def create_welcome_message(sender, **kwargs):
    if kwargs.get('created', False):
        Notification.objects.create(user=kwargs.get('instance'),
                                    title="Welcome to Ecto Blog! Click here to get started",
                                    message="Thanks for signing up!")
                                    
@receiver(friendship_request_accepted)
def friendship_accepted(sender, **kwargs):
    General_notification.objects.create(user=kwargs.get('to_user'),
                                        title="Friend request accepted",
                                        message="you are now friends with %s" % kwargs.get('from_user'))

@receiver(friendship_request_accepted)
def friendship_accepted(sender, **kwargs):
    General_notification.objects.create(user=kwargs.get('from_user'),
                                        title="Friend request accepted",
                                        message="%s accepted your friend request" % kwargs.get('to_user'))

