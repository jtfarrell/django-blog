# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0002_auto_20150116_1821'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='likes_cheese',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
