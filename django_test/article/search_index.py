import datetime
from article.models import Article
from haystack import indexes

class ArticleIndex(indexes.SearchIndex, indexes.Indexable): # this means we can use as search indez and go throught and create indexes
    text = indexes.CharField(document=True, use_template=True)
    pub_date = indexes.DateTimeField(model_attr='pub_date')
    
    content_auto = indexes.EdgeNgranField(model attr='title') # model attribute is paying attention to the title
    
    def get_model(self):
        return Article
    
    def index_queryset(self. using=None)
        return self.get_model().objects.all()