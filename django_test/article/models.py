from django.db import models
from time import time
from approvals.models import Approval
from django.dispatch import receiver
from approvals.signals import article_approved
from django.db.models import signals
from django.conf import settings
from django_test import settings
from django.db.models.signals import post_save
import os.path
from whoosh.index import create_in, open_dir



import logging
logr = logging.getLogger(__name__)

def get_upload_file_name(instance, filename):
    return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'), filename)

class Article(models.Model):
    title = models.CharField(max_length= 200)
    body = models.TextField()
    pub_date = models.DateTimeField('date published')
    likes = models.IntegerField(default=0)
    thumbnail = models.FileField(upload_to=get_upload_file_name)
    approved = models.BooleanField(default=False)
    
    def get_absolute_url(self):
        return "/articles/get/%i/" % self.id 
    
    
    def __unicode__(self):
        return self.title

@receiver(post_save, sender=Article)       
def create_approval_on_new_article(sender, **kwargs):
    if kwargs.get('created', False):
        approval = Approval.objects.create(article_id=kwargs.get('instance').id)
        logr.debug("Approval created: %s" % approval)


@receiver(article_approved)   
def approve_article(sender, **kwargs):
    a = Article.objects.get(id=kwargs.get('article_id'))
    a.approved = True
    a.save()
    logr.debug("Approved received")


class Comment(models.Model):
    name = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField('date published')
    article = models.ForeignKey(Article) # links the to model functions together