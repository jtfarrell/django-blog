from django import template

register = template.Library() # template library

@register.filter(name='article_shorten_body') # registers custom filter
def article_shorten_body(bodytext, length):
    if len(bodytext) > length:
        text = "%s ..." % bodytext[0:length]
    else:
        text = bodytext
    
    return text