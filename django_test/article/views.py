from django.shortcuts import render_to_response, get_object_or_404, render
from article.models import  Article, Comment
from django.http import HttpResponse
from forms import ArticleForm, CommentForm
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.utils import timezone
from haystack.query import SearchQuerySet # allows us to do searches within out views
from django.template import RequestContext # allos to pass info like message to one page to another -- unlike render_to_response
from django.contrib import messages
from django.conf import settings

from .tasks import hello_world

def articles(request):
    language = 'en-gb'
    session_language = 'en-gb'
    
    if 'lang' in request.COOKIES:
        language = request.COOKIES['lang']
    
    if 'lang' in request.session:
        session_language = request.session['lang']
        
    args = {}
    args.update(csrf(request))
    
    args['articles'] = Article.objects.all()
    args['language'] = language
    args['session_language'] = session_language
    hello_world.delay()
    
    return render(request,'articles.html', args)
                             
    
def article(request, article_id):
    #x = get_object_or_404(Article, pk=article_id)
    #return render_to_response('article.html', {'article': x})
    
    return render(request, 'article.html',
                  {'article': Article.objects.get(id=article_id)})
    
    
def language(request, language='en-gb'):
    response = HttpResponse("setting language to %s" % language)
    
    response.set_cookie('lang', language)
    
    request.session['lang'] = language
    return response

def create(request):
    if request.POST:
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            c = form.save(commit=False) # saves form but doesn't enter it into the database
            c.pub_date = timezone.now()
            c.save()
            
            messages.success(request, "Your Article was added")
            
            return HttpResponseRedirect('/articles/all')
            
    else:
        form = ArticleForm()
        
    args = {}
    args.update(csrf(request))
    
    args['form'] = form
    
    return render_to_response('create_article.html', args)
    
def delete_comment(request, comment_id):
    c = Comment.objects.get(id=comment_id)
    
    article_id = c.article.id
    
    c.delete()
    
    messages.add_message(request,
                         settings.DELETE_MESSAGE,
                         "Your comment was deleted")
    
    return HttpResponseRedirect("/articles/get/%s" % article_id)
    
def like_article(request, article_id):
    if article_id:
        a = Article.objects.get(id=article_id)
        count = a.likes # put the number of likes into count
        count += 1 # add 1 to count
        a.likes = count # put the integer from count back into the likes
        a.save()
        
    return HttpResponseRedirect('/articles/get/%s' % article_id)
    
def add_comment(request, article_id):
    a = Article.objects.get(id=article_id)
    
    if request.method == "POST":
        f = CommentForm(request.POST)
        if f.is_valid():
            c = f.save(commit=False) # saves form but doesn't enter it into the database
            c.pub_date = timezone.now()
            c.article = a
            c.save()
            
            messages.success(request, "Your comment was added") # shorthand success message
        
            return HttpResponseRedirect('/articles/get/%s' % article_id)
        
    else:
		f = CommentForm()
	
		args = {}
		args.update(csrf(request))

		args['article'] = a
		args['form'] = f

		return render_to_response('add_comment.html', args)
		
def search_titles(request):
    articles = SearchQuerySet().autocomplete(content_auto=request.POST.get('search_text', ''))

    return render_to_response('ajax_search.html', {'articles' : articles})
    

    