import django.dispatch # pulls in the django frameworkthat allows to build signals

article_approved = django.dispatch.Signal(providing_args=['article_id'])