# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0009_remove_comment_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='user_id',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='user',
        ),
    ]
