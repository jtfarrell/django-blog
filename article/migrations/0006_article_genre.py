# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0005_vote'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='genre',
            field=models.CharField(default=b'', max_length=200),
            preserve_default=True,
        ),
    ]
