# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0003_auto_20150130_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='user_id',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='user',
            field=models.TextField(default=b''),
            preserve_default=True,
        ),
    ]
